window.onload = function() {
    // Código a ejecutar cuando se ha cargado toda la página
  document.body.style.visibility = "visible";
   // stage2.stop();
    main();
};
var audios = [{
    url: "./sounds/click.mp3",
    name: "clic"
}];
ivo.info({
    title: "Bienestar Universitario - Generalidades",
    autor: "Edilson Laverde Molina",
    date: "06-05-2023",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});


import scaleStage from './scale.js';
var stage1 = new TimelineMax();
var pop = new TimelineMax();
function main() {
    var t = null;
    var udec = ivo.structure({
        created: function () {

            t = this;
			scaleStage();
            t.ini();
            t.animations();
            t.events();

            //precarga audios//
            var onComplete = function () {
				stage1.play();
				//agregamos el active al body
				document.body.classList.add("active");
            };
            ivo.load_audio(audios,onComplete );
        },
        methods: {

			ini:function(){
				this.popupActive(1);
				
				
			},
			popupActive:function(indexActive){
				//ocultamos todos los elementos
				let arrayFondos = document.querySelectorAll("#popup_fondo > img");
				let arrayPopups = document.querySelectorAll("#popup_container > div");
				for (let i = 0; i < arrayFondos.length; i++) {
					const element = arrayFondos[i];
					element.style.display = "none";
				}
				for (let i = 0; i < arrayPopups.length; i++) {
					const element = arrayPopups[i];
					element.style.display = "none";
				}
				//mostramos el elemento activo
				document.getElementById("popup_fondo"+indexActive).style.display = "block";
				document.getElementById("popup_container"+indexActive).style.display = "flex";

			},
            events: function () {
				var _this = this;
                ivo("#stage1_main_btn1").on("click", function () {
                    ivo.play("clic");
					_this.popupActive(1);
					pop.timeScale(1).play();
                });
				ivo("#stage1_main_btn2").on("click", function () {
					ivo.play("clic");
					_this.popupActive(2);
					pop.timeScale(1).play();
				});
				ivo("#stage1_main_btn3").on("click", function () {
					ivo.play("clic");
					_this.popupActive(3);
					pop.timeScale(1).play();
				});
				ivo("#stage1_main_btn4").on("click", function () {
					ivo.play("clic");
					_this.popupActive(4);
					pop.timeScale(1).play();
				});
				ivo("#stage1_main_btn5").on("click", function () {
					ivo.play("clic");
					_this.popupActive(5);
					pop.timeScale(1).play();
				});
				ivo("#popup_close").on("click", function () {
					ivo.play("clic");
					pop.timeScale(6).reverse();
				});
                
            },
            animations: function () {

                stage1.append(TweenMax.from("#stage1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from("#stage1_title", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from("#stage1_main", .8, {y: -20, opacity: 0}), 0);
				stage1.append(TweenMax.staggerFrom("#stage1_main img", .8, {x: 100, opacity: 0}, .2), 0);
                stage1.stop();

				
				pop.append(TweenMax.from("#popup", 1.2, {x: 1300, opacity: 0}), 0);
				pop.append(TweenMax.from("#popup_close", .5, {x: 30,rotation:900, opacity: 0}), 0);
				pop.append(TweenMax.staggerFrom("#popup_container", .8, {x: 100, opacity: 0}, .2), 0);
				pop.stop();
                
				
				/*
                let movil = false;
                let windowWidth = window.innerWidth;
                if (windowWidth < 1024) {
                    movil = true;
                    
                }
                if('ontouchstart' in window || navigator.maxTouchPoints) {
                    movil = true;
                }
                if (movil) {
                    stage1.play();
                    stage2.play();
                    stage3.play();
                }*/

            }
        }
    });
}