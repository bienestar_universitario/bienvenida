function scaleStage() {
	//cambiamos el valos de una variable css para movil y desktop
	// Seleccione el elemento app y establezca un alias a una variable para hacer referencia al elemento en el futuro.
	var stage = document.querySelector("#app");
	stage.style.transformOrigin = "0 0";
	stage.style.msTransformOrigin = "0 0";
	stage.style.webkitTransformOrigin = "0 0";
	stage.style.mozTransformOrigin = "0 0";
	stage.style.oTransformOrigin = "0 0";
	// Seleccione el elemento padre de app y establezca un alias a una variable para hacer referencia al elemento en el futuro.
	var parent = stage.parentElement;
	// Obtenga el ancho del elemento padre de app.
	var parentWidth = parent.offsetWidth;
	// Obtenga la altura de la ventana del navegador.
	var parentHeight = window.innerHeight;
	// Obtenga el ancho del elemento app.
	var stageWidth = stage.offsetWidth;
	// Obtenga la altura del elemento app.
	var stageHeight = stage.offsetHeight;
	// Establezca el nuevo ancho del elemento app alrededor del 100% del ancho del elemento padre.
	var desiredWidth = Math.round(parentWidth * 1);
	// Establezca la nueva altura del elemento app alrededor del 100% de la altura de la ventana del navegador.
	var desiredHeight = Math.round(parentHeight * 1);
	// Establezca la variable rescaleWidth para calcular la nueva escala del elemento app según su ancho.
	var rescaleWidth = (desiredWidth / stageWidth);
	// Establezca la variable rescaleHeight para calcular la nueva escala del elemento app según su altura.
	var rescaleHeight = (desiredHeight / stageHeight);
	// Establezca la variable rescale en rescaleWidth a menos que la altura del elemento app escalado sea mayor que la altura de la ventana del navegador, en cuyo caso use rescaleHeight.
	var rescale = rescaleWidth;
	if (stageHeight * rescale > desiredHeight) {
	rescale = rescaleHeight;
	}
	// Establezca la propiedad transform del elemento app en la nueva escala.
	stage.style.transform = "scale(" + rescale + ")";
	stage.style.oTransform = "scale(" + rescale + ")";
	stage.style.msTransform = "scale(" + rescale + ")";
	stage.style.webkitTransform = "scale(" + rescale + ")";
	stage.style.mozTransform = "scale(" + rescale + ")";
	stage.style.oTransform = "scale(" + rescale + ")";
	// Restablezca la altura del elemento padre para que los elementos debajo se reorganicen según la nueva altura.
	parent.style.height = stageHeight * rescale + "px";
	// Establezca el margen izquierdo del elemento app para que se centre horizontalmente en la ventana del navegador.
	var center = (window.innerWidth - stage.offsetWidth * rescale) / 2;
	stage.style.left = center + "px";
	// Establezca el margen superior del elemento app para que se centre verticalmente en la ventana del navegador.
	var middle = (window.innerHeight - stage.offsetHeight * rescale) / 2;
	stage.style.top = middle + "px";
}
// establecemos un evento para cuando cambie de tamaño la ventana
window.addEventListener("resize", scaleStage);

export default scaleStage;